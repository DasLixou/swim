use serde::Deserialize;

pub trait NextpnrDevice {
    fn package_flag(&self) -> &str;
}

#[derive(Deserialize, Clone, Debug)]
pub enum Ice40Device {
    #[serde(rename = "iCE40LP384")]
    Ice40lp384,
    #[serde(rename = "iCE40LP1K")]
    Ice40lp1k,
    #[serde(rename = "iCE40LP4K")]
    Ice40lp4k,
    #[serde(rename = "iCE40LP8K")]
    Ice40lp8k,
    #[serde(rename = "iCE40HX1K")]
    Ice40hx1k,
    #[serde(rename = "iCE40HX4K")]
    Ice40hx4k,
    #[serde(rename = "iCE40HX8K")]
    Ice40hx8k,
    #[serde(rename = "iCE40UP3K")]
    Ice40up3k,
    #[serde(rename = "iCE40UP5K")]
    Ice40up5k,
    #[serde(rename = "iCE5LP1K")]
    Ice5lp1k,
    #[serde(rename = "iCE5LP2K")]
    Ice5lp2k,
    #[serde(rename = "iCE5LP4K")]
    Ice5lp4k,
}

impl NextpnrDevice for Ice40Device {
    fn package_flag(&self) -> &str {
        match self {
            Ice40Device::Ice40lp384 => "--lp384",
            Ice40Device::Ice40lp1k => "--lp1k",
            Ice40Device::Ice40lp4k => "--lp4k",
            Ice40Device::Ice40lp8k => "--lp8k",
            Ice40Device::Ice40hx1k => "--hx1k",
            Ice40Device::Ice40hx4k => "--hx4k",
            Ice40Device::Ice40hx8k => "--hx8k",
            Ice40Device::Ice40up3k => "--up3k",
            Ice40Device::Ice40up5k => "--up5k",
            Ice40Device::Ice5lp1k => "--u1k",
            Ice40Device::Ice5lp2k => "--u2k",
            Ice40Device::Ice5lp4k => "--u4k",
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
pub enum Ecp5Device {
    #[serde(rename = "LFE5U-12F")]
    Lfe5u12f,
    #[serde(rename = "LFE5U-25F")]
    Lfe5u25f,
    #[serde(rename = "LFE5U-45F")]
    Lfe5u45f,
    #[serde(rename = "LFE5U-85F")]
    Lfe5u85f,
    #[serde(rename = "LFE5UM-25F")]
    Lfe5um25f,
    #[serde(rename = "LFE5UM-45F")]
    Lfe5um45f,
    #[serde(rename = "LFE5UM-85F")]
    Lfe5um85f,
    #[serde(rename = "LFE5UM5G-25F")]
    Lfe5um5g25f,
    #[serde(rename = "LFE5UM5G-45F")]
    Lfe5um5g45f,
    #[serde(rename = "LFE5UM5G-85F")]
    Lfe5um5g85f,
}

impl NextpnrDevice for Ecp5Device {
    fn package_flag(&self) -> &str {
        match self {
            Ecp5Device::Lfe5u12f => "--12k",
            Ecp5Device::Lfe5u25f => "--25k",
            Ecp5Device::Lfe5u45f => "--45k",
            Ecp5Device::Lfe5u85f => "--85k",
            Ecp5Device::Lfe5um25f => "--um-25k",
            Ecp5Device::Lfe5um45f => "--um-45k",
            Ecp5Device::Lfe5um85f => "--um-85k",
            Ecp5Device::Lfe5um5g25f => "--um5g-25k",
            Ecp5Device::Lfe5um5g45f => "--um5g-45k",
            Ecp5Device::Lfe5um5g85f => "--um5g-85k",
        }
    }
}
