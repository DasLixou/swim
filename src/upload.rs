use std::process::Command;

use color_eyre::eyre::{anyhow, Context, Result};
use log::info;

use crate::{
    config::UploadTool,
    packing::{run_packing, PackingResult},
    CommandCtx,
};

fn upload_command(upload_config: &UploadTool, use_sudo: bool) -> Command {
    if use_sudo {
        info!(
            "Attempting to run {binary} as sudo",
            binary = upload_config.binary()
        );
        let mut command = Command::new("sudo");
        command.arg(upload_config.binary());
        command
    } else {
        Command::new(upload_config.binary())
    }
}

pub fn upload(ctx: &CommandCtx, use_sudo: bool) -> Result<()> {
    let packing_result = run_packing(ctx)?;
    let upload_config = ctx.config.upload_config()?;

    match upload_config.as_ref() {
        UploadTool::Icesprog | UploadTool::Iceprog | UploadTool::Tinyprog => {
            let bin_file = match packing_result {
                PackingResult::Bin(bin_file) => bin_file,
                _ => {
                    return Err(anyhow!(
                        "{} only supports bin-files",
                        upload_config.binary()
                    ))
                }
            };

            info!("Uploading bin-file");

            let extra_args = match upload_config.as_ref() {
                UploadTool::Tinyprog => vec!["-p"],
                _ => vec![],
            };

            let status = upload_command(&upload_config, use_sudo)
                .args(extra_args)
                .arg(bin_file)
                .status()
                .with_context(|| format!("Failed to run {}", upload_config.binary()))?;

            if status.success() {
                info!("Upload successful");
                Ok(())
            } else {
                Err(anyhow!("Failed to program device"))
            }
        }
        UploadTool::OpenOcd { config_file } => {
            let svf_file = match packing_result {
                PackingResult::Svf(svf_file) => svf_file,
                _ => return Err(anyhow!("openocd only supports svf-files")),
            };

            let status = upload_command(&upload_config, use_sudo)
                .arg("-f")
                .arg(config_file)
                .arg("-c")
                .arg("init")
                .arg("-c")
                .arg(format!("svf -quiet {}", svf_file))
                .arg("-c")
                .arg("exit")
                .status()
                .context("Failed to run openocd")?;

            if status.success() {
                info!("Upload successful");
                Ok(())
            } else {
                Err(anyhow!("Failed to program device"))
            }
        }
        UploadTool::Fujprog => {
            let svf_file = match packing_result {
                PackingResult::Svf(svf_file) => svf_file,
                _ => return Err(anyhow!("openocd only supports svf-files")),
            };

            let status = upload_command(&upload_config, use_sudo)
                .arg(svf_file)
                .args(&["-T", "svf"])
                .status()
                .context("Failed to run openocd")?;

            if status.success() {
                info!("Upload successful");
                Ok(())
            } else {
                Err(anyhow!("Failed to program device"))
            }
        }
        UploadTool::Custom { commands } => {
            for command in commands {
                let command = command.replace("#packing_result#", packing_result.file().as_str());

                info!("Running `{command}`");
                let status = upload_command(&upload_config, use_sudo)
                    .arg("-c")
                    .arg(&command)
                    .status()
                    .with_context(|| format!("Failed to run `sh -c '{command}'`"))?;

                if !status.success() {
                    return Err(anyhow!("Custom command `{command}` failed"));
                }
            }

            info!("Upload successful");
            Ok(())
        }
    }
}
